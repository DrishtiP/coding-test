import { TestBed } from '@angular/core/testing';

import { IntToAplhabetConverterService } from './int-to-aplhabet-converter.service';

describe('IntToAplhabetConverterService', () => {
  let service: IntToAplhabetConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IntToAplhabetConverterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
