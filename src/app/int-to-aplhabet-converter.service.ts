import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IntToAplhabetConverterService {
  alphabetArray;

  constructor() { }

  convertToAplhabet(val) {
    this.alphabetArray = [];
    let splitArray = val.split('#');
    console.log(splitArray);
    splitArray.map(item => {
      if (item.length) {
        let alphabet = item % 26;
        const leveller = 65;
        this.alphabetArray.push(String.fromCharCode(alphabet + leveller));
        alphabet = 0;
      }

    })
    console.log(this.alphabetArray.join(''));
    return this.alphabetArray.join('');
  }
}
