import { IntToAplhabetConverterService } from './../int-to-aplhabet-converter.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-converter',
  templateUrl: './app-converter.component.html',
  styleUrls: ['./app-converter.component.scss']
})
export class AppConverterComponent implements OnInit {
  totalNumbers;
  clickValue = '';
  intValue: string;
  numberArray = [];
  alphabetValue = ''
  constructor(private converter: IntToAplhabetConverterService) { }

  ngOnInit(): void {
    this.totalNumbers = [
      { value: 1, displayName: 1 },
      { value: 2, displayName: 2 },
      { value: 3, displayName: 3 },
      { value: 4, displayName: 4 },
      { value: 5, displayName: 5 },
      { value: 6, displayName: 6 },
      { value: 7, displayName: 7 },
      { value: 8, displayName: 8 },
      { value: 9, displayName: 9 },
      { value: '#', displayName: '#' },
      { value: 0, displayName: 0 },
      { value: 'remove', displayName: '<--' },]
  }

  clickAction(element) {
    if (element.value == 'remove') {
      if (this.numberArray.length > 0) {
        this.numberArray.pop();
        this.clickValue = this.numberArray.join('');
      }
    }
    else {
      alert("You typed " + element.value);
      this.numberArray.push(element.value);
      this.clickValue = this.numberArray.join('');
    }
    if (this.clickValue) {
      this.alphabetValue = this.converter.convertToAplhabet(this.clickValue.trim());
    }
    else {
      this.alphabetValue = 'Text';
    }
  }
}
